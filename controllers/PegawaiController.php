<?php

namespace app\controllers;

use yii;
use app\models\Pegawai;
use app\models\Personal;
use app\models\PegawaiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * PegawaiController implements the CRUD actions for Pegawai model.
 */
class PegawaiController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['update'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pegawai models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pegawai model.
     * @param int $id_pegawai Id Pegawai
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_pegawai)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_pegawai),
        ]);
    }

    /**
     * Creates a new Pegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pegawai();
        $jenis_pegawai = Pegawai::JENIS_PEGAWAI;
        $status_pegawai = Pegawai::STATUS_PEGAWAI;
      
        $nama_personal = Personal::getAllPersonal();
        // echo "<pre>";
        // var_dump($nama_personal); die();
       
        if ($model->load($this->request->post())){

            $model->tgl_bergabung = \Yii::$app->formatter->asDate($model->tgl_bergabung,'yyyy-MM-dd');
            $model->save();
            
        return $this->redirect(['view', 'id_pegawai' => $model->id_pegawai]);
            
       }

        return $this->render('create', [
            'model' => $model,
            'jenis_pegawai'=>$jenis_pegawai,
            'status_pegawai'=>$status_pegawai,
            'nama_personal'=>$nama_personal,
        ]);
    }

    /**
     * Updates an existing Pegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_pegawai Id Pegawai
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_pegawai)
    {
        $model = $this->findModel($id_pegawai);
        $jenis_pegawai = Pegawai::JENIS_PEGAWAI;
        $status_pegawai = Pegawai::STATUS_PEGAWAI;
        $nama_personal = Personal::getAllPersonal();

  
        // $post = Yii::$app->request->post('Pegawai');
        // $tgl_bergabung = \Yii::$app->formatter->asDate($post['tgl_bergabung'],'yyyy-MM-dd');

        if ($model->load($this->request->post())){
        $model->tgl_bergabung = \Yii::$app->formatter->asDate($model->tgl_bergabung,'yyyy-MM-dd');
        $model->save();
            
        return $this->redirect(['view', 'id_pegawai' => $model->id_pegawai]);
            
       }

        return $this->render('update', [
            'model' => $model,
           'jenis_pegawai'=>$jenis_pegawai,
            'status_pegawai'=>$status_pegawai,
            'nama_personal'=>$nama_personal,

        ]);
    }


    /**
     * Deletes an existing Pegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_pegawai Id Pegawai
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_pegawai)
    {
        $this->findModel($id_pegawai)->delete();
        Yii::$app->getSession()->setFlash('danger', [
            'text' => 'Deleted',
            'title' => 'Data Berhasil di Hapus',
            'type' => 'error',
            'timer' => 10000,
            'showConfirmButton' => false
        ]); 
        return $this->redirect(['index']);
    }

    /**
     * Finds the Pegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_pegawai Id Pegawai
     * @return Pegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_pegawai)
    {
        if (($model = Pegawai::findOne(['id_pegawai' => $id_pegawai])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
