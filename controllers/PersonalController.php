<?php

namespace app\controllers;

use yii;
use app\models\Personal;
use app\models\Pegawai;
use app\models\PersonalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PersonalController implements the CRUD actions for Personal model.
 */
class PersonalController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Personal models.
     *
     * @return string
     */
    public function actionIndex()
    {   
        
        $searchModel = new PersonalSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        // $dataProvider->query->where(['jenis_kelamin'=>'laki-laki']);//selec where jenis_kelamin = 'laki-laki'
   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Personal model.
     * @param int $id_personal Id Personal
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_personal)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_personal),
        ]);
    }

    /**
     * Creates a new Personal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Personal();
        $modelPegawai = new Pegawai();
        $statusPerkawinan = Personal::STATUS_PERKAWINAN;
        $agama = Personal::AGAMA;
        $pendidikan = Personal::PENDIDIKAN;

        $jenis_pegawai = Pegawai::JENIS_PEGAWAI;
        $status_pegawai = Pegawai::STATUS_PEGAWAI;
        $nama_personal = Personal::getAllPersonal();
        
        // echo "<pre>";
        // print_r($_POST);die();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $modelPegawai->load($this->request->post())){
              
              $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->tgl_lahir = \Yii::$app->formatter->asDate($model->tgl_lahir,'yyyy-MM-dd'); 
                    if (!$model->save()) {
                        throw new \Exception('Gagal menyimpan Personal : ' .json_encode($model->getErrors()));
                        
                    }


                    $modelPegawai->id_personal = Yii::$app->db->getLastInsertID();
                    $modelPegawai->tgl_bergabung = \Yii::$app->formatter->asDate($modelPegawai->tgl_bergabung,'yyyy-MM-dd');
                    
                    if (!$modelPegawai->save()) {
                       throw new \Exception('Gagal menyimpan Pegawai :' .json_encode($modelPegawai->getErrors()));
                    }

                    Yii::$app->getSession()->setFlash('success', [
                        'text' => 'Sukses !',
                        'title' => 'Data Berhasil Disimpan',
                        'type' => 'success',
                        'timer' => 5000,
                        'showConfirmButton' => false
                    ]);
                    $transaction->commit();
                    return $this->redirect(['view', 'id_personal' => $model->id_personal]);
                } catch (\Exception $e) {
                    $transaction->rollBack();
                   
                     Yii::$app->getSession()->setFlash('error', [
                        'text' =>  $e->getMessage(),
                        'title' => 'Data Gagal Disimpan',
                        'type' => 'error',
                        'timer' => 10000,
                        'showConfirmButton' => false
                    ]);
                     return $this->redirect(Yii::$app->request->referrer);
                    // throw $e;
                }
               
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelPegawai'=>$modelPegawai,
            'statusPerkawinan'=>$statusPerkawinan,
            'agama'=>$agama,
            'pendidikan'=>$pendidikan,
            'nama_personal'=>$nama_personal,
            'jenis_pegawai'=>$jenis_pegawai,
            'status_pegawai'=>$status_pegawai,
        ]);
    }

    /**
     * Updates an existing Personal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_personal Id Personal
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_personal)
    {
        $statusPerkawinan = Personal::STATUS_PERKAWINAN;
        $agama = Personal::AGAMA;
        $pendidikan = Personal::PENDIDIKAN;
        $model = $this->findModel($id_personal);
        $modelPegawai = new Pegawai();
        $jenis_pegawai = Pegawai::JENIS_PEGAWAI;
        $status_pegawai = Pegawai::STATUS_PEGAWAI;
        $model->tgl_lahir = date('d-M-Y',strtotime($model->tgl_lahir));
        // var_dump($model->tgl_lahir); die();
        
            if ($model->load($this->request->post())){
                $model->tgl_lahir = \Yii::$app->formatter->asDate($model->tgl_lahir,'yyyy-MM-dd');
             
                $model->save();
                return $this->redirect(['view', 'id_personal' => $model->id_personal]);
            }
        

        return $this->render('update', [
            'model' => $model,
            'modelPegawai' => $modelPegawai,
            'jenis_pegawai' => $jenis_pegawai,
            'status_pegawai' => $status_pegawai,
            'statusPerkawinan'=>$statusPerkawinan,
            'agama'=>$agama,
            'pendidikan'=>$pendidikan,
        ]);
    }

    /**
     * Deletes an existing Personal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_personal Id Personal
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_personal)
    {
        $this->findModel($id_personal)->delete();
        Yii::$app->getSession()->setFlash('error', [
            'text' =>  'Deleted',
            'title' => 'Data Berhasil Dihapus',
            'type' => 'error',
            'timer' => 10000,
            'showConfirmButton' => false
        ]);                            
        return $this->redirect(['index']);
    }

    /**
     * Finds the Personal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_personal Id Personal
     * @return Personal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_personal)
    {
        if (($model = Personal::findOne(['id_personal' => $id_personal])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
