<?php

use yii\db\Migration;

/**
 * Class m220821_170548_create_berkas_pegawai
 */
class m220821_170548_create_berkas_pegawai extends Migration
{
    /**
     * {@inheritdoc}
     */
   public function up()
    {
        $this->createTable('berkas_pegawai', [
            'id_berkas_pegawai' => $this->primaryKey(),
            'id_pegawai' => $this->integer()->notNull(),
            'jenis_identitas' => $this->string(30)->notNull(),
            'no_identitas' => $this->string(50)->notNull(),
            'tanggal_akhir_valid' => $this->date(),
        ]);
        $this->addForeignKey(
            'fk-berkas_pegawai_id_pegawai',
            'berkas_pegawai',//nama tabel
            'id_pegawai',
            'pegawai',
            'id_pegawai',
            'category',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('berkas_pegawai');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220821_170548_create_berkas_pegawai cannot be reverted.\n";

        return false;
    }
    */
}
