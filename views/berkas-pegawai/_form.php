<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\BerkasPegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="berkas-pegawai-form">
    
   
    <div class="x_panel">
    <div class="x_title">
        <h2>Berkas Pegawai<!--  <small>different form elements</small> --></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a class="dropdown-item" href="#">Settings 1</a>
                    </li>
                    <li><a class="dropdown-item" href="#">Settings 2</a>
                    </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
<br>



<div class="x_content">
 <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($pegawai, 'id_pegawai')->textInput(['value' => $pegawai->personal->nama_lengkap, 'disabled'=>true])->label('Nama Pegawai') ?>
        </div>
   
        <div class="col-md-4">
            <?= $form->field($model, 'jenis_identitas')->widget(Select2::classname(), [
                'data' => $jenis_identitas,
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>

        
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'no_identitas')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'tanggal_akhir_valid')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Pilih tanggal bergabung'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' =>'dd-M-yyyy'
                ]]);
            ?>
           
        </div>        
    </div>
    <div class="row">
         <div class="col-md-4">
            <?=  $form->field($model, 'file_berkas')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*','pdf/*'],
                 'pluginOptions' =>
                    [ 
                        'showUpload'=>false,
                        'allowedFileExtensions' => ['jpg','jpeg','png','pdf'],
                        /*'maxFileSize' => 100*/
                    ],
            ])
            ?>    
        </div>
    </div>
    <br>
    <div class="x_title">
    </div>
    <br>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    </div>
    </div>
</div>
