<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pegawais';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pegawai-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pegawai', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_pegawai',
            // 'id_personal',
            // 'personal.nama_lengkap',
             [
                'attribute'=>'nama_pegawai',
                'value'=>function($model){

                    return $model->personal->nama_lengkap;
                }
                
            ],
            
             [
                'attribute'=>'personal.jenis_kelamin',
                'headerOptions'=>['style'=>'width:140px'],
                'filter'=>Html::activeDropdownlist($searchModel,'jenis_kelamin',\app\models\Personal::JENIS_KELAMIN,['class'=>'form-control','prompt'=>'pilih'])
                 
            ],
            // [
            //     'attribute'=>'alamat',
            //     'headerOptions'=>['style'=>'width:150px'],
            //     'value'=>function($model){

            //         return $model->personal->alamat;
            //     }
            // ],
            [
                'attribute'=>'no_ktp',
                'value'=>function($model){

                    return $model->personal->no_ktp;
                }
            ],
            // 'jenis_pegawai',
             [
                'attribute'=>'jenis_pegawai',
                // 'headerOptions'=>['style'=>'width:140px'],
                'filter'=>Html::activeDropdownlist($searchModel,'jenis_pegawai',\app\models\Pegawai::JENIS_PEGAWAI,['class'=>'form-control','prompt'=>'pilih'])
                 
            ],
            'status_pegawai',
            'jabatan',
            'tgl_bergabung',
            'gaji',
            [
                'header' => 'Aksi',
                'class' => ActionColumn::className(),
                'headerOptions'=>['style'=>'width:100px'],
                'template' =>  '{view} {update} {berkas} {delete}',
                'buttons' => 
                    [
                        'view' => function($url, $model){
                            $url = Yii::$app->urlManager->createUrl(['pegawai/view', 'id' => $model->id_pegawai]);
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>',$url,['tittle' => 'View']);
                        },
                        'update' => function($url, $model){
                            $url = Yii::$app->urlManager->createUrl(['pegawai/update', 'id' => $model->id_pegawai]);
                            return Html::a('<i class="glyphicon glyphicon-pencil"></i>',$url,['tittle' => 'Update']);
                        },
                        'berkas' => function($url, $model){
                            $url = Yii::$app->urlManager->createUrl(['berkas-pegawai/create', 'id' => $model->id_pegawai]);
                            return Html::a('<i class="glyphicon glyphicon-file"></i>',$url,['tittle' => 'Add File']);
                        },
                        'delete' => function($url, $model){
                            $url = Yii::$app->urlManager->createUrl(['pegawai/view', 'id' => $model->id_pegawai]);
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>',$url,[
                                'tittle' => 'Delete',
                                'data-confirm' => Yii::t('yii', 'Apakah yakin ingin menghapus data ini?'),
                                'data-method' => 'post',
                            ]);
                        },
                       

                    ],
                // 'urlCreator' => function ($action, \app\models\Pegawai $model, $key, $index, $column) {
                //     return Url::toRoute([$action, 'id_pegawai' => $model->id_pegawai]);
                //  }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
