<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Pegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin(); ?>

   
     <div class="row">
        <div class="col-md-4">
            
            <?php 
                // Usage with ActiveForm and model
            echo $form->field($model, 'id_personal')->widget(Select2::classname(), [
                'data' => $nama_personal,
                'options' => ['placeholder' => 'Pilih Nama ...'],
                // 'theme'=>Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($model, 'jenis_pegawai')->widget(Select2::classname(), [
                'data' => $jenis_pegawai,
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            ?>
        </div>
         <div class="col-md-4">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($model, 'status_pegawai')->widget(Select2::classname(), [
                'data'=>$status_pegawai,
                 'options' => ['placeholder' => 'Pilih status pegawai ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
             <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($model, 'tgl_bergabung')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Pilih Tanggal...'],
                'pluginOptions' => [
                'autoclose' => true,
                'format' =>'dd-M-yyyy'
                ],
            ]);

            ?>
        </div>
         <div class="col-md-4">
             <?= $form->field($model, 'gaji')->textInput(['type'=>'number']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
