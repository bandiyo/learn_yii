<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>
        
        <!--field merupakan sebuah label-->
    <?= $form->field($model, 'name') ?> <!--name tidak boleh beda dengan-->
    <?= $form->field($model, 'email') ?><!--yang sudah di deklare di model-->

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
