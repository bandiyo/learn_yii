<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use app\widgets\Alert;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use app\assets\AppAsset;
use aryelds\sweetalert\SweetAlert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login" >
<?php $this->beginBody(); ?>
<div class="container body">
        <div class="container">
            <?php 
            foreach (Yii::$app->session->getAllFlashes() as $message) {
               echo SweetAlert::widget([
                'options' => [
                    'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
                    'text' => (!empty($message['text'])) ? Html::encode($message['text']) : 'Text Not Set!',
                    'type' => (!empty($message['type'])) ? $message['type'] : SweetAlert::TYPE_INFO,
                    'timer' => (!empty($message['timer'])) ? $message['timer'] : 5000,
                    'showConfirmButton' =>  (!empty($message['showConfirmButton'])) ? $message['showConfirmButton'] : true
                ]
            ]);
           }?>
         <div class="col-md-4"></div>
         <div class="col-md-4"><?= $content ?></div>
     </div>
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
