<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Personal */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="x_panel">
    <div class="x_title">
      <h2>Identitas Personal<!--  <small>different form elements</small> --></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
            </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <br>
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'nama_lengkap')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'nama_panggilan')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'jenis_kelamin')->radioList(['Laki-laki' => 'Laki-laki','Perempuan'=>'Perempuan'],['item'=>function($index, $label, $name, $checked, $value){

                return '<label style="margin-right:30px"><input type="radio" class="flat" name="'.$name.'" value="'.$value.'"'.($checked?"checked":"").'>'.$label.'</label>';

            }]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tempat_lahir')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <!-- <?= $form->field($model, 'tgl_lahir')->textInput() ?> -->
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($model, 'tgl_lahir')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Pilih tanggal'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' =>'dd-M-yyyy'
                ]
            ]);

            ?>
        </div>
        <div class="col-md-3">
            <!-- <?= $form->field($model, 'status_perkawinan')->textInput(['maxlength' => true]) ?> -->
            <?php 
                // Usage with ActiveForm and model
            echo $form->field($model, 'status_perkawinan')->widget(Select2::classname(), [
                'data' => $statusPerkawinan,
                'options' => ['placeholder' => 'Pilih status pernikahan ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
           <!-- <?= $form->field($model, 'agama')->textInput(['maxlength' => true]) ?> -->

            <?php 
                // Usage with ActiveForm and model
            echo $form->field($model, 'agama')->widget(Select2::classname(), [
                'data' => $agama,
                'options' => ['placeholder' => 'Pilih Agama ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
       </div>
       <div class="col-md-3">
        <!-- <?= $form->field($model, 'pendidikan')->textInput(['maxlength' => true]) ?> -->
          <?php 
                // Usage with ActiveForm and model
            echo $form->field($model, 'pendidikan')->widget(Select2::classname(), [
                'data' => $pendidikan,
                'options' => ['placeholder' => 'Pilih Pendidikan ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">       
        <?= $form->field($model, 'no_ktp')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
       <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>
   </div>
   <div class="col-md-3">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
</div>
</div>



                                                               
</div>                                
</div>


<div class="x_panel">
 <div class="x_title">
      <h2>Data Pegawai<!--  <small>different form elements</small> --></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a class="dropdown-item" href="#">Settings 1</a>
                </li>
                <li><a class="dropdown-item" href="#">Settings 2</a>
                </li>
            </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="x_content">
<div class="pegawai-form">

 

   
     <div class="row">
     
        <div class="col-md-6">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($modelPegawai, 'jenis_pegawai')->widget(Select2::classname(), [
                'data' => $jenis_pegawai,
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            ?>
        </div>
         <div class="col-md-6">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($modelPegawai, 'status_pegawai')->widget(Select2::classname(), [
                'data'=>$status_pegawai,
                 'options' => ['placeholder' => 'Pilih status pegawai ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
             <?= $form->field($modelPegawai, 'jabatan')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php
                // Usage with model and Active Form (with no default initial value)
            echo $form->field($modelPegawai, 'tgl_bergabung')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Pilih Tanggal...'],
                'pluginOptions' => [
                'autoclose' => true,
                'format' =>'dd-M-yyyy'
                ],
            ]);

            ?>
        </div>
         <div class="col-md-4">
             <?= $form->field($modelPegawai, 'gaji')->textInput(['type'=>'number']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>



