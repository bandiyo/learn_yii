<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Personal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_personal',
            // 'nama_lengkap',
            // 'nama_panggilan',
            [
                'header'=>'Nama Lengkap',
                'headerOptions'=>['style'=>'width :200px','class'=>'text-center'],
                'contentOptions'=>['class'=>'text-center'],
                'value'=>function($model){
                    // echo "<pre>";
                    // print_r($model);
                    // die();
                    return $model->nama_lengkap;
                }
            ],
            'jenis_kelamin',
            'tempat_lahir',
            // 'tgl_lahir',
            [
                'header'=>'Tgl. Lahir',
                'headerOptions'=>['style'=>'width :200px','class'=>'text-center'],
                'contentOptions'=>['class'=>'text-center'],
                'value'=>function($model){
                    // echo "<pre>";
                    // print_r($model);
                    // die();
                   $result =$model->tgl_lahir;
                   return date("d-M-Y",strtotime($result));
                }
            ],
            'no_ktp',
            'no_telp',
            // 'status_perkawinan',
            // 'agama',
            // 'pendidikan',
            // 'alamat',

            // 'email:email',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, \app\models\Personal $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_personal' => $model->id_personal]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
