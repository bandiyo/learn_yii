<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $model app\models\Personal */

$this->title = 'Create Personal';
$this->params['breadcrumbs'][] = ['label' => 'Personals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'modelPegawai'=>$modelPegawai,
		'statusPerkawinan'=>$statusPerkawinan,
		'agama'=>$agama,
		'pendidikan'=>$pendidikan,
		'nama_personal'=>$nama_personal,
		'jenis_pegawai'=>$jenis_pegawai,
		'status_pegawai'=>$status_pegawai,
	]) ?>

</div>
