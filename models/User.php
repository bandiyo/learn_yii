<?php

namespace app\models;
use yii\db\ActiveRecord;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;
     public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time_create', 'time_update', 'id_pegawai'], 'safe'],
            ['status','default','value' => self::STATUS_ACTIVE],
            ['status','in', 'range'=>[self::STATUS_ACTIVE, self::STATUS_DELETE]],
            
        ];
    }

    /**
     * {@inheritdoc}
     */


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {   //self fungsinya sama seperti $this untuk memanggil variabel, namun self digunakan untuk memanggil static variabel
        return static::findOne(['id'=>$id,'status'=>self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
         self::find()->where(['accessToken' => $token])->all();
        foreach ($users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       return static::findOne(['username'=>$username,'status'=>self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
